const express = require("express");
const app = express();
const path = require("path");
const term = require("terminal-kit").terminal;
const { body, validationResult, matchedData } = require("express-validator");
const cors = require("cors");
const multer = require("multer");
require("dotenv").config();

// File configuration storage
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, fileDirectory);
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    file.mimetype.split(path.sep)[1];
    cb(
      null,
      file.fieldname +
        "-" +
        uniqueSuffix +
        "." +
        file.mimetype.split(path.sep)[1]
    );
  },
});

const upload = multer({ storage: storage });
// END FILE UPLOAD

const router = express.Router();
const fileDirectory = path.join(path.dirname(__dirname), "public");
const { orderTicket, findTickets } = require("./routes/orderTicket/order");
// MongoDB Connection
const { client } = require("./libs/adminDB");

// EXPRESS MIDDLEWARES
app.use(cors());
app.use(express.static(fileDirectory));
app.use(express.json());
// END

/* ***********************************
    POST     /order_ticket
    Endpoint receives user's data.
*************************************/
router.post(
  "/order_ticket",
  upload.single("identificationCard"),
  body("surName").notEmpty().escape(),
  body("otherName").notEmpty().escape(),
  body("dateOfBirth").escape(),
  body("phoneNumber").isNumeric().isLength({ max: 12 }).escape(),
  body("countryCity").notEmpty().escape(),
  body("identificationNumber").isNumeric().escape(),
  body("meetPurpose").notEmpty().escape(),
  (req, res) => {
    const result = validationResult(req);
    if (result.isEmpty()) {
      const data = matchedData(req);
      return orderTicket(req, res, { ...data, avatar_url: req.file.filename });
    }
    res.status(406).json({ errors: result.array() });
  }
);

router.get("/order_list", (req, res) => {
  return findTickets(req, res);
});

router.get("/", (req, res) => {
  res.send("Tickety API! \n");
});
app.use("/api", router);

// Initiate MongoDB Connection
async function admin() {
  await client.connect();
  console.log("Connected MongoDB database successfully to server");

  app.listen(process.env.PORT, process.env.HOSTNAME, () => {
    term.yellow.bold(
      `Server running on http://${process.env.HOSTNAME}:${process.env.PORT}\n`
    );
  });

  return "done";
}

admin().then(console.log).catch(console.error);
