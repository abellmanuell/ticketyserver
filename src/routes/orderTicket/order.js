const { getCollection } = require("../../libs/adminDB");
const term = require("terminal-kit").terminal;

// Find a list by id number
const findById = async function (idNumber) {
  const collection = await getCollection("order_list");
  const result = await collection
    .find({ identificationNumber: idNumber })
    .toArray();
  return [...result];
};

// Create a order
const createOrder = async function (data) {
  const collection = await getCollection("order_list");
  const result = await collection.insertOne(data);
  return result;
};

// Initiate Ticket Order
async function orderTicket(req, res, data) {
  const { identificationNumber } = data;
  const isFindOrder = await findById(identificationNumber);

  if (isFindOrder.length) {
    term.red(`${identificationNumber} ordered already tickcet!\n`);

    return res.status(400).json({
      msg: `${identificationNumber} ordered already tickcet!`,
      identificationNumber,
    });
  }

  await createOrder(data);
  res.setHeader("Content-type", "text/html");
  res.send(
    `
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Ticket Ordered</title>
      <style type="text/css">
        h1{
          text-align: center;
          color: #32a85a;
        }
    
        section{
          text-align: center;
          padding: 5rem;
        }
    
        div{
          font-size: 4rem;
        }
    
      </style>
    </head>
    <body>
      <section>
        <div>&#128077;</div>
    
        <h1>Successfully Ticket Ordered!</h1>
        <p>Congratulations! Your ticket order has been successfully placed. <br />Get ready for an amazing experience!</p>
        <a href="/">Back Home</a>
      </section>
    </body>
    </html>
    `
  );
  term.bold("Ticket ordered successfully!\n");
}

// Lookup for all tickets
async function findTickets(req, res) {
  const collection = await getCollection("order_list");
  const result = await collection.find({}).toArray();
  return res.json(result);
}

module.exports = { orderTicket, findTickets };
