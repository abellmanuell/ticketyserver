const { MongoClient } = require("mongodb");
const client = new MongoClient(process.env.DB_URL);
const term = require("terminal-kit").terminal;

async function getDB(databaseName) {
  await client.connect();
  const db = client.db(databaseName);
  term.green.bold("Access Database!\n");
  return db;
}

async function getCollection(collection) {
  const coll = await (await getDB("tickety")).collection(collection);
  term.blue.bold("Got the collection!\n");
  return coll;
}

module.exports = { client, getCollection, getDB };
